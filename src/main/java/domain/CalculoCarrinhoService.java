package domain;



public class CalculoCarrinhoService {

    public ResumoCarrinho calcular(Carrinho carrinho) {

        var totalCarrinho = carrinho.getTotal();
        var quantidadeItens = carrinho.getItems().size();
        var frete = 0.0;

        if (quantidadeItens > 3 && quantidadeItens < 5)
            totalCarrinho = totalCarrinho - (totalCarrinho * 0.05);
        else if (quantidadeItens >= 5)
            totalCarrinho = totalCarrinho - (totalCarrinho * 0.08);

        frete = totalCarrinho > 50.0 ? 0 : 9.0;


        return new ResumoCarrinho(totalCarrinho, frete, quantidadeItens);
    }
}
