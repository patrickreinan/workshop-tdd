package domain;

public class Item {

    private final double valor;
    private final int quantidade;

    public Item(double valor, int quantidade) {

        this.valor = valor;
        this.quantidade = quantidade;
    }

    public double total(){
        return valor * quantidade;
    }
}
