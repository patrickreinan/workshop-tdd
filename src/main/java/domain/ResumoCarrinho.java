package domain;

public class ResumoCarrinho {

    public ResumoCarrinho(double total, double frete, int qtdeItens) {
        this.total = total;
        this.frete = frete;
        this.qtdeItens = qtdeItens;
    }

    public double getTotal() {
        return total;
    }

    public double getFrete() {
        return frete;
    }

    public int getQtdeItens() {
        return qtdeItens;
    }

    private final double total;
    private final double frete;
    private final int qtdeItens;


}
