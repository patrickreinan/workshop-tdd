package domain;

import java.util.ArrayList;

public class Carrinho {


    private final ArrayList<Item> items;

    public Carrinho() {
        this.items = new ArrayList<>();
    }

    public double getTotal() {
        double total = 0.0;
        for (var item  :items) {
            total += item.total();
        }
        return total;
    }


    public ArrayList<Item> getItems(){
        return this.items;
    }

    public void addItem(Item item) {
        this.items.add(item);
    }


}
