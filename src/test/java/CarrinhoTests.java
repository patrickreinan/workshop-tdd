import domain.CalculoCarrinhoService;
import domain.Carrinho;
import domain.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
Se tiver mais do que 3 itens (itens diferentes, não quantidade total), tem desconto de 5%
Se tiver mais do que 5 itens (itens diferentes, não quantidade total), tem desconto de 8%
Se o valor total for mais do 50 reais (com os descontos acima) tem frete  grátis, se não o frete é de 9.00
 */

public class CarrinhoTests {

    private final CalculoCarrinhoService service;
    private final  Carrinho carrinho;

    public CarrinhoTests() {
        service =new  CalculoCarrinhoService();
        carrinho = new Carrinho();
    }

    @Test

    public void carrinho_tem_desconto_sobre_itens() {

        //arrange...
        carrinho.addItem(new Item(3.5, 2));
        carrinho.addItem(new Item(5.5, 5));
        carrinho.addItem(new Item(2.5, 6));
        carrinho.addItem(new Item(4.5, 4));

        var totalEsperado = 64.125;
        //act...
        var resumoCarrinho = service.calcular(carrinho);

        //assert...
        assertEquals(totalEsperado, resumoCarrinho.getTotal());

    }

    @Test
    public void carrinho_nao_tem_desconto_sobre_itens() {
        //arrange...
        carrinho.addItem(new Item(3.5, 1));

        var totalEsperado = 3.5;
        //act...
        var resumoCarrinho = service.calcular(carrinho);

        //assert...
        assertEquals(totalEsperado, resumoCarrinho.getTotal());
    }

    @Test
    public void carrinho_tem_frete() {
        //arrange...
        carrinho.addItem(new Item(10, 1));

        var freteEsperado = 9.0;

        //act...
        var resumoCarrinho = service.calcular(carrinho);

        //assert...
        assertEquals(freteEsperado, resumoCarrinho.getFrete());
    }

    @Test
    public void carrinho_nao_tem_frete() {
        //arrange...
        carrinho.addItem(new Item(60, 1));

        var freteEsperado = 0.0;

        //act...
        var resumoCarrinho = service.calcular(carrinho);

        //assert...
        assertEquals(freteEsperado, resumoCarrinho.getFrete());
    }
}
