import domain.CalculoCarrinhoService;
import domain.Carrinho;
import domain.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(value = Parameterized.class)
public class CalculoCarrinhoServiceTests {

    private final double freteEsperado;
    private final double totalEsperado;
    private final Item[] items;

    @Parameterized.Parameters(name = "{index}: frete {1} - total esperado {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{

                {
                        new Item[]{
                                new Item(2.0, 1),
                                new Item(4.0, 8),
                                new Item(5.0, 8),
                                new Item(5.0, 5),
                        },
                        0,
                        94.05
                },
                {
                        new Item[]{
                                new Item(2.0, 1),
                        },
                        9.0,
                        2
                },
                {
                        new Item[]{
                                new Item(5.0, 1),
                                new Item(4.0, 2),
                                new Item(3.0, 3),
                                new Item(2.0, 4),
                                new Item(1.0, 5),
                                new Item(5.0, 6),
                        },
                        0,
                        59.8
                }
        });
    }

    public CalculoCarrinhoServiceTests(Item[] items, double freteEsperado, double totalEsperado) {
        this.items = items;
        this.freteEsperado = freteEsperado;
        this.totalEsperado = totalEsperado;
    }

    @Test
    public void calcular_tests() {

        var carrinho = new Carrinho();
        var service = new CalculoCarrinhoService();

        for (Item item : items) {
            carrinho.addItem(item);
        }

                //act...
        var resumoCarrinho = service.calcular(carrinho);

        //assert...
        assertEquals(totalEsperado, resumoCarrinho.getTotal());

    }
}
